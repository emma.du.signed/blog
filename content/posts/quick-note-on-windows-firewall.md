---
title: "Quick Note on Windows Firewall"
date: 2022-01-15T21:36:07+08:00
draft: false
---
# Why
Customers who used google cloud with windows server often face firewall problems,
but it can actually be quick resolved with a simple command line with windows netsh
command utility.
